# require expander

Reads input file and replace `require('module-name')` to souce code of `module-name`

# Example

file *main.lua*
```
require("lib/my-lib")

function main()
  myLib:printGreetings()
end
```

file *lib/my-lib.lua*
```
myLib = {
  printGreetings = function() 
    print('Hello, world!')
  end
}
```

after run `requireExpander main.lua` we'll get `main.expanded.lua` file with next content:

```
-- require("lib/my-lib")
myLib = {
  printGreetings = function()
    print('Hello, world!')
  end
}


function main()
  myLib:printGreetings()
end
```

# Supported languages

* LUA

# Usage

* install  
  `npm i https://gitlab.com/xevin/require-directive-expander.git`
  
* add to `package.json` to `"script"` section
  ```
  "dev": "requireExpander main.lua",
  "build": "requireExpander main.lua --replace"
  ```
  
* run `npm run build` or `npm run dev`
