
import Parser from './Parser.js'

export default class LuaParser extends Parser {
  comment = '--'
  regex = new RegExp(/^require\(['"](.*)['"]\)/)
  extension = 'lua'
}
