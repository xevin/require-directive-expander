import LuaParser from './LuaParser.js'

function parse() {
  const parser = new LuaParser('./test.lua', {replaceCode: true})
  parser.writeTo('out.lua')
}

parse()
