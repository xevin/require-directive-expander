import * as fs from 'fs'
import * as readline from 'readline'
import * as path from 'path'


export default class Parser {
  comment = '' // is a single line comment chars like '//', '#', '--' etc

  /*
    regex is for extract module path from 'require()' code string;
    see getModuleName()
   */
  regex = new RegExp()
  extension = '' // module files extension like 'js', 'lua' etc

  _baseDir = ''
  _nodeList = []
  _options = {
    replaceCode: false
  }

  constructor(filePath, options) {
    this._options = options || this._options
    this.fileName = path.basename(filePath)
    this._baseDir = path.dirname(filePath)
  }

  isRequireNode(codeString) {
    return codeString.match(this.regex)
  }

  isModuleExists(path) {
    return fs.existsSync(path)
  }

  getModuleName(codeLine) {
    // this method must convert 'require("namespace/module")' to 'namespace/module'
    return codeLine.replace(this.regex, '$1')
  }

  getModuleFilename(moduleName) {
    return `${moduleName}.${this.extension}`
  }

  getModulePath(filename) {
    return path.join(this._baseDir, filename)
  }

  getModuleContent(path) {
    return fs.readFileSync(path, 'utf8')
  }

  parseNode(code) {
    let newLine = `${code}\n`

    const newNode = {
      code: newLine
    }

    if (this.isRequireNode(code)) {
      const moduleName = this.getModuleName(code)
      const moduleFilename = this.getModuleFilename(moduleName)
      newNode.path = this.getModulePath(moduleFilename)

      if (this.isModuleExists(newNode.path)) {
        newNode.expandCode = this.getModuleContent(newNode.path) + '\n'
      } else {
        newNode.expandCode = `${this.comment} module '${newNode.path}' not found in '${path.dirname(newNode.path)}'\n\n`
      }
    }

    return newNode
  }

  parseNodes() {
    return new Promise((resolve, reject) => {
      const file = readline.createInterface({
        input: fs.createReadStream(this.fileName)
      })

      file.on('line', (line) => {
        this._nodeList.push(this.parseNode(line))
      })

      file.on('error', (error) => {
        reject(error)
      })

      file.on('close', () => {
        resolve(this._nodeList)
      })
    })
  }

  writeTo (path) {
    if (fs.existsSync(path)) {
      fs.unlinkSync(path)
    }

    this.parseNodes()
      .then(() => {
        this._nodeList.forEach(node => {
          let newLine = node.code

          if (node.expandCode) {
            if (this._options.replaceCode) {
              newLine = node.expandCode
            } else {
              newLine = `${this.comment} ${newLine}${node.expandCode}`
            }
          }

          fs.writeFileSync(path, newLine, { flag: 'a+'})
        })
      })
      .catch((err) => {
        console.log(err)
      })
  }
}
