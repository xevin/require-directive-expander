#!/usr/bin/env node -r esm

import * as path from 'path'
import LuaParser from '../src/LuaParser'


function main () {
  const args = process.argv.slice(2)

  if (args.length > 0) {
    const fin = args[0]

    const ext = path.extname(fin)
    const fname = fin.replace(ext, '')

    const replaceCodeArg = args.includes('--replace-code')
    const options = replaceCodeArg ? { replaceCode: true } : null
    let parser = new LuaParser(args[0], options)

    parser.writeTo(`${fname}.expanded${ext}`)
  } else {
    console.log(`requireExpander usage:

    requireExpander source-file.lua [--replace-code]`)
  }
}

main()
